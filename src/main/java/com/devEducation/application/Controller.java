package com.devEducation.application;

import com.devEducation.dto.SongDTO;
import com.devEducation.model.Song;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private final ObservableList<String> genres = FXCollections.observableArrayList();

    @FXML
    private final ComboBox<String> comboGenre = new ComboBox<>();

    ObservableList<String> singer = FXCollections.observableArrayList();
    @FXML
    private final ComboBox<String> comboSinger = new ComboBox<>();

    @FXML
    private final TableView<Song> table = new TableView<>();

    @FXML
    public Button seeSongs;

    @FXML
    private TableColumn<Song, String> columnSong;

    @FXML
    private TableColumn<Song, String> columnSinger;

    @FXML
    private TableColumn<Song, String> columnGenre;

    @FXML
    private TableColumn<Song, String> columnYear;

    @FXML
    private TableColumn<Song, String> columnTime;

    MediaPlayer player;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        columnSong.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnSinger.setCellValueFactory(new PropertyValueFactory<>("singer"));
        columnGenre.setCellValueFactory(new PropertyValueFactory<>("genre"));
        columnYear.setCellValueFactory(new PropertyValueFactory<>("year"));
        columnTime.setCellValueFactory(new PropertyValueFactory<>("time"));

        table.setRowFactory(tv -> {
            TableRow<Song> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1) {
                    String uriString = new File(table.getSelectionModel().getSelectedItem().getLink().replaceAll("\\?", "")).toURI().toString();
                    player = new MediaPlayer(new Media(uriString));
                    player.play();
                }
            });
            return row;
        });
    }

    @FXML
    private void open() {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        File dir = directoryChooser.showDialog(null);
        List<String> genres = SongDTO.setLink(String.valueOf(dir).replaceAll("\\\\", "/"));
        System.out.println(genres.toString());
        this.genres.addAll(genres);
        comboGenre.setItems(this.genres);
    }

    @FXML
    private void actionComboGenre() {
        singer.clear();
        List<String> singers = SongDTO.getSingersByGenre(comboGenre.getValue());
        this.singer.addAll(singers);
        singer.addAll();
        comboSinger.setItems(singer);
    }

    @FXML
    private void actionComboArtist() {
        List<Song> songs = SongDTO.getSongBySinger(comboGenre.getValue(), comboSinger.getValue());
        List<Song> songDtoList = new ArrayList<>();
        for (Song song : songs) {
            songDtoList.add(new Song(song.getName(), song.getGenre(), song.getSinger(), song.getLink(), song.getTime()));
        }
        ObservableList<Song> songData = FXCollections.observableArrayList();
        songData.addAll(songDtoList);
        table.setItems(songData);
    }

    @FXML
    private void play() {
        player.play();
    }

    @FXML
    private void stop() {
        player.pause();
    }

}