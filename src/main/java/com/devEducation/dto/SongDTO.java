package com.devEducation.dto;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.devEducation.model.Song;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.valueOf;

public class SongDTO {

    private static final Logger logger = LoggerFactory.getLogger(SongDTO.class);

    public static List<String> setLink(String path) {
        List<String> genres = new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/addSong?url=" + path;
            logger.info(url);
            Jsoup.connect(url).ignoreContentType(true).get();
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            logger.info(doc.text());
            genres = Arrays.asList(new Gson().fromJson(valueOf(doc.text()), String.class).split(","));
        } catch (Exception e) {
            logger.error("");
        }
        return genres;
    }

    public static List<String> getSingersByGenre(String genre) {
        List<String> singers = new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/getSingers?genre=" + genre;
            logger.info(url);
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            logger.info(doc.text());
            singers = Arrays.asList(new Gson().fromJson(valueOf(doc.text()), String.class).split(","));
        } catch (Exception e) {
            logger.error("");
        }
        return singers;
    }

    public static List<Song> getSongBySinger(String genre, String singer) {
        List<Song> songs = new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/getSong?genre=" + genre + "&singer=" + singer;
            logger.info(url);
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            logger.info(doc.text());
            songs = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Song>>() {
                    }.getType());
        } catch (Exception e) {
            logger.error("");
        }
        return songs;
    }
}
