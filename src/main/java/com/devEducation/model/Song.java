package com.devEducation.model;

public class Song {

    private String name;

    private String genre;

    private String singer;

    private String link;

    private String time;

    public Song(String name, String genre, String singer, String link, String time) {
        this.name = name;
        this.genre = genre;
        this.singer = singer;
        this.link = link;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return "Song{name='" + name + ", genre='" + genre + ", singer='" + singer + '}';
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
