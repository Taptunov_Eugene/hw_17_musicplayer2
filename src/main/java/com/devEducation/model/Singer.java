package com.devEducation.model;

public class Singer {

    private String genre;
    private String name;

    public Singer(String genre, String artists) {
        this.genre = genre;
        this.name = artists;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSingers() {
        return name;
    }

    public void setSingers(String artists) {
        this.name = artists;
    }

    @Override
    public String toString() {
        return "Artist{genre='" + genre + ", singers='" + name + '}';
    }
}
